package latitude

import (
	"math/rand"
	"testing"
)

func TestMin(t *testing.T) {
	tables := []struct {
		x float32
		y float32
		n float32
	}{
		{0.0, 0.0, 0.0},
		{0, 1, 0},
		{1, 0, 0},
		{-1, 1, -1},
		{0.00000001, 0.00000000001, 0.00000000001},
		{-1, -2, -2},
	}

	for _, table := range tables {
		actual := min(table.x, table.y)
		if actual != table.n {
			t.Errorf("min of (%f, %f) was incorrect, got: %f, want: %f.", table.x, table.y, actual, table.n)
		}
	}
}
func TestMax(t *testing.T) {
	tables := []struct {
		x float32
		y float32
		n float32
	}{
		{0.0, 0.0, 0.0},
		{0, 1, 1},
		{1, 0, 1},
		{-1, 1, 1},
		{0.00000001, 0.00000000001, 0.00000001},
		{-1, -2, -1},
	}

	for _, table := range tables {
		actual := max(table.x, table.y)
		if actual != table.n {
			t.Errorf("max of (%f, %f) was incorrect, got: %f, want: %f.", table.x, table.y, actual, table.n)
		}
	}
}

func TestGetMaxProfit(t *testing.T) {
	tables := []struct {
		x []float32
		n float32
	}{
		{nil, 0},
		{make([]float32, 0), 0},
		{[]float32{9, 9}, 0},
		{[]float32{9, 10}, 1},
		{[]float32{-2, -1}, 0},
		{[]float32{-1, 1}, 0},
		{[]float32{-1, -2, 1, 2, 5}, 4},
		{[]float32{9, 9}, 0},
		{[]float32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 9},
		{[]float32{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, 0},
		{[]float32{10, 7, 5, 8, 11, 9}, 6},
		{[]float32{10.1, 7, 5, 8, 11.5, 9}, 6.5},
	}
	for _, table := range tables {
		actual := GetMaxProfit(table.x)
		if actual != table.n {
			t.Errorf("GetMaxProfit of (%v) was incorrect, got: %f, want: %f.", table.x, actual, table.n)
		}
	}
}

func BenchmarkGetMaxProfit(b *testing.B) {
	stockPriceYesterday := make([]float32, 8*60)
	for i := range stockPriceYesterday {
		stockPriceYesterday[i] = rand.Float32()
	}
	for n := 0; n < b.N; n++ {
		GetMaxProfit(stockPriceYesterday)
	}
}
