package latitude

import "log"

/**
  GetMaxProfit is to calculate the max profit gained from yesterday's stock price data
  All input prices are expected to be positive, non-positive values will be disgarded
  If no profit can be gained, instead of profit loss(negative value), 0 will be returned
*/
func GetMaxProfit(stockPriceYesterday []float32) (maxProfit float32) {
	if len(stockPriceYesterday) == 0 || len(stockPriceYesterday) == 1 {
		log.Printf("Input stock prices is either nil or empty\n")
		return maxProfit
	}
	currentMin := float32(0)
	for index, price := range stockPriceYesterday {
		// discard non-positive price during processing
		if price <= 0 {
			log.Printf("Non-positive value found at index [%d]: %f\n", index, price)
			continue
		}
		// initialize current min to the first positive price in the list
		if currentMin == 0 {
			currentMin = price
			continue
		}
		maxProfit, currentMin = max(maxProfit, price-currentMin), min(currentMin, price)
	}
	return maxProfit
}

func min(a, b float32) float32 {
	if a < b {
		return a
	}
	return b
}

func max(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}
